<?php if (!empty($pre_object)) print render($pre_object) ?>

<div class='<?php print $classes ?> clearfix' <?php print ($attributes) ?>>
  <?php if (!empty($title_prefix)) print render($title_prefix); ?>

    <h2 <?php if (!empty($title_attributes)) print $title_attributes ?>>
      <?php if (!empty($new)): ?><span class='new'><?php print $new ?></span><?php endif; ?>
      <?php print $title ?>
    </h2>

  <?php if (!empty($title_suffix)) print render($title_suffix); ?>



  <?php if (!empty($content)): ?>
    <div class='<?php print $hook ?>-content clearfix<?php if (!empty($is_prose)) print 'prose' ?>'>
      <?php hide($content['field_tags'])?>
      <?php print render($content) ?>
      <div class="grid_6 alpha">
          <?php print render($content['field_tags']) ?>
      </div>
    </div>
  <?php endif; ?>

  <?php if (!empty($links)): ?>
    <div class='<?php print $hook ?>-links clearfix'>
      <?php print render($links) ?>
    </div>
  <?php endif; ?>
</div>

<div class="grid_8 alpha">
    <?php if (!empty($post_object)) print render($post_object) ?>
</div>
