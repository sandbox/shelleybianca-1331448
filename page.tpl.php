<div id="wrapper" class="container_16">
    <div id="header" class="grid_16">
        <h1><a href="<?php print $front_page; ?>"><?php print render($site_name); ?></a></h1>
        <h2><?php print render($site_slogan); ?></h2>
        <?php print render($page['header']); ?>
    </div>

    <div id="main-menu" class="grid_16 arial">
        <?php print theme('links__system_main_menu', array(
            'links' => $main_menu,
            'attributes' => array(
                'id' => 'main-menu',
                'class' => array(
                    'links',
                    'inline',
                    'clearfix'
                    )
                )
            )); ?>
    </div>
    
    <?php if ($is_front)
        $content_width = 'grid_16';
    ?>
    
    <div id="content" class="<?php print render($content_width); ?>">
       <?php print render($page['content']); ?>
    </div>
        
    <?php if (!$is_front): ?>
        <div id="sidebar" class="grid_4 omega">
            <?php print render($page['sidebar']); ?>     
        </div>
    <?php endif; ?>
       
    <div class="clear"></div>
    
    <div id="footer" class="grid_16 arial">
        <div id="footer-1" class="grid_4">
          <?php print render($page['footer_1']); ?>
        </div>
        <div id="footer-2" class="grid_4">
          <?php print render($page['footer_2']); ?>
        </div>
        <div id="footer-3" class="grid_4">
          <?php print render($page['footer_3']); ?>
        </div>
    </div>
</div>